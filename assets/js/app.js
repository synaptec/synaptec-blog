$(document).ready(function() {

  'use strict';

  // ==========================================
  // Add no-touch class if touch is not enabled
  // ==========================================

  if (!('ontouchstart' in document.documentElement)) {
    $('html').addClass('no-touch');
  } else {
    $('html').addClass('has-touch');
  }

  // =========================================================
  // Order header tags inside the dropdown menu alphabetically
  // http://stackoverflow.com/a/30095613/558777
  // =========================================================

  var tags_list = $('.nav__dropdown--tag-list');

  tags_list.children().detach().sort(function(a, b) {
    return $(a).text().localeCompare($(b).text());
  }).appendTo(tags_list);

  // ===================
  // Navigation Dropdown
  // ===================

  $('.nav__link--has-dropdown').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    $(this).find('.nav__dropdown').toggleClass('nav__dropdown--is-active');
  });

  $('.nav__dropdown').click(function(e) {
    e.stopPropagation();
  });

  $(document).click(function(event) {
    if ($('.nav__dropdown').hasClass('nav__dropdown--is-active')) {
      // If the target is a URL, just hide the dropdown
      if($(event.target).closest('a').length) {
        $('.nav__dropdown').removeClass('nav__dropdown--is-active');
        return false;
      }

      $('.nav__dropdown').removeClass('nav__dropdown--is-active');
    }
  });

  // ===============
  // Off Canvas menu
  // ===============

  $('.off-canvas-toggle').click(function(e) {
    e.preventDefault();
    $('.off-canvas-container').toggleClass('off-canvas-container--is-active');
  });

  // =====================
  // Koenig Gallery
  // =====================
  var gallery_images = document.querySelectorAll('.kg-gallery-image img');

  gallery_images.forEach(function (image) {
    var container = image.closest('.kg-gallery-image');
    var width = image.attributes.width.value;
    var height = image.attributes.height.value;
    var ratio = width / height;
    container.style.flex = ratio + ' 1 0%';
  });

  // =================
  // Responsive videos
  // =================

  $('.wrapper').fitVids({
    'customSelector': ['iframe[src*="ted.com"]']
  });

  // ==========================
  // Reading Time for Post Card
  // ==========================

  $('.post-card__read-time').each(function() {
    $(this).readingTime({
      readingTimeTarget: $(this).find('.post-card__read-time-time')
    });
  });

  // ============================
  // Reading Time for Single Post
  // ============================

  $('.post__content').readingTime();

  // Read the value from .eta class and update post header reading time element
  var eta = $('.eta').text();
  $('.post__read-time-time').text(eta);

  // =====================
  // Numbered Pagination
  // https://goo.gl/6cytqp
  // =====================

  // Grab the total pages value
  var page_total = $('.page-total').text();

  // Grab the current page value
  var page_current = $('.page-current').text();

  // Grab the pagination span element
  var numbered_pagination = document.getElementById('numbered-pagination');

  if (page_total > 1) {
    // For each number, from 1 to the total number of pages, add in the page number and a link to the page
    for (var i = 1; i <= page_total; i++) {
      // If you are on the current page, bold the nubmer and do not link
      if (i == page_current) {
        numbered_pagination.innerHTML = numbered_pagination.innerHTML + "<li class='numbered-pagination__item numbered-pagination__item--current'>" + i + "</li>";
      } else {
        // Otherwise, add a link to the page
        // This is where all the customization can go, space between each link, commas, etc.
        numbered_pagination.innerHTML = numbered_pagination.innerHTML + "<li class='numbered-pagination__item'><a class='numbered-pagination__link' href=/page/" + i + ">" + i + "</a></li>";
      }
    }
  }

});